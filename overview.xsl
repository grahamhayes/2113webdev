<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<body>
				<table>
					<thead>
						<tr>
							<th>Student ID</th>
							<th>Surname</th>
							<th>Forname</th>
							<th>Number of Modules</th>
						</tr>
					</thead>
					<tbody>
						<xsl:for-each select="students/student">
							<tr>
								<td><xsl:value-of select="@studentId"/></td>
								<td><xsl:value-of select="name/lastname"/></td>
								<td><xsl:value-of select="name/firstname"/></td>
								<td><xsl:value-of select='count(modules/module)'/></td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>