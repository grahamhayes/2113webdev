<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<body>
				<table>
					<thead>
						<tr>
							<th>Student Number</th>
							<th>Module 1</th>
							<th>Grade</th>
							<th>Module 2</th>
							<th>Grade</th>
							<th>Module 3</th>
							<th>Grade</th>
							<th>Module 4</th>
							<th>Grade</th>
						</tr>
					</thead>
					<tbody>
						<xsl:for-each select="students/student">
							<tr>
								<td><xsl:value-of select="@studentId"/></td>
								<xsl:for-each select="modules/module">
									<td><xsl:value-of select="modulecode"/></td>
									<td><xsl:value-of select="grade"/></td>
								</xsl:for-each>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>